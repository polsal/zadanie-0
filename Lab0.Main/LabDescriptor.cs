﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(aparaty_fotograficzne);
        public static Type B = typeof(lustrzanki);
        public static Type C = typeof(kompakty);

        public static string commonMethodName = "wysun_obiektyw";
    }
}
