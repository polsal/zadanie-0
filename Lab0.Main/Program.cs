﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.WriteLine("Wybierz aparat:");
                Console.WriteLine("1. Aparat kompaktowy");
                Console.WriteLine("2. Lustrzanka cyfrowa");
                Console.WriteLine("Wpisz cyfrę:");
                string wybor = Convert.ToString(Console.ReadLine());
                Console.WriteLine();
                if (wybor == "1")
                {
                    Console.WriteLine("Wybrałeś kompakt");
                    kompakty aparat = new kompakty();
                    int obiektyw = aparat.wysun_obiektyw();
                    Console.WriteLine("Kompakt ma obiektyw o długości " + obiektyw + "mm");
                    Console.ReadKey();
                }
                else if (wybor == "2")
                {
                    Console.WriteLine("Wybrałeś lustrzankę");
                    lustrzanki aparat = new lustrzanki();
                    int obiektyw = aparat.wysun_obiektyw();
                    Console.WriteLine("Lustrzanka ma obiektyw o długości " + obiektyw + "mm");
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("błędny wybór, wybierz jeszcze raz");
                }
                Console.WriteLine();
            } while (true);           
        }
    }
}
